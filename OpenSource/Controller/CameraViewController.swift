//
//  CameraViewController.swift
//  OpenSource
//
//  Created by 加藤直行 on 2015/12/09.
//  Copyright © 2015年 加藤直行. All rights reserved.
//

import UIKit

/**
 * カメラ画面クラス
 */
class CameraViewController: UIViewController {

    /**
     * カメラView
     */
    @IBOutlet weak var cameraView:CameraView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
