//
//  CameraView.swift
//  OpenSource
//
//  Created by 加藤直行 on 2015/12/09.
//  Copyright © 2015年 加藤直行. All rights reserved.
//

import UIKit
import AVFoundation

/**
 * カメラViewクラス
 */
class CameraView: UIView {
    
    /**
     * カメラセッション
     */
    var mySession : AVCaptureSession!
    
    /**
     * カメラデバイス
     */
    var myDevice : AVCaptureDevice!
    
    /**
     * カメラアウトプット
     */
    var myImageOutput : AVCaptureStillImageOutput!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        mySession = AVCaptureSession()
        let devices = AVCaptureDevice.devices()
        
        for device in devices{
            if(device.position == AVCaptureDevicePosition.Back) {
                myDevice = device as! AVCaptureDevice
            }
        }
        
        let videoInput: AVCaptureInput!
        do {
            videoInput = try AVCaptureDeviceInput.init(device: myDevice!)
        } catch {
            videoInput = nil
        }
        
        mySession.addInput(videoInput)
        myImageOutput = AVCaptureStillImageOutput()
        mySession.addOutput(myImageOutput)
        
        let screenSize: CGSize = UIScreen.mainScreen().bounds.size
        
        let myVideoLayer : AVCaptureVideoPreviewLayer = AVCaptureVideoPreviewLayer.init(session:mySession)
        myVideoLayer.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        myVideoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        
        self.layer.addSublayer(myVideoLayer)
        
        mySession.startRunning()
        
    }
    
    /**
     * 撮影
     *
     * @param button ボタン
     */
    @IBAction func photoGraphing(button: UIButton){
        let myVideoConnection = myImageOutput.connectionWithMediaType(AVMediaTypeVideo)
        
        self.myImageOutput.captureStillImageAsynchronouslyFromConnection(myVideoConnection, completionHandler: { (imageDataBuffer, error) -> Void in
            
            let myImageData : NSData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataBuffer)
            
            let myImage : UIImage = UIImage(data: myImageData)!
            
            UIImageWriteToSavedPhotosAlbum(myImage, self, nil, nil)
        })
    }
}
